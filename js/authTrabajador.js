//#region Variables globales
/*VARIABLES ESTATISCAS*/
var ligaCase = 'php/caseBancoppel.php';
var promotor = {};
var datosPagina = {};
var datosServidor = {};
var urlParams = {};
var intentos = {};
var cTituloModal = 'AUTENTICACIÓN BANCOPPEL';
var RUTA_HUELLAS_INE = '/sysx/progs/web/entrada/huellas/bancoppel/';
var RUTA_LOCAL_HUELLAS = 'C:\\TEMP\\huellas';
const rutas = {
    RUTA_HUELLAS_BANCOPPEL : '/sysx/progs/web/entrada/huellas/bancoppel/',
    RUTA_LOCAL_HUELLAS : 'C:\\TEMP\\huellas'
};
/*VARIABLES SERVICIOWEBX*/
var iContHuellas = 0;   
const CAPTURA_INDICES_BANCOPPEL = 2;      // "C:\\SYS\\PROGS\\CAPTURAINDICESINEAFILIACION.EXE"
const SUBIR_ARCHIVO_FTP_SVR = 3;          // "C:\\SYS\\PROGS\\SUBIRARCHIVOFTPSVR.EXE"
var VALIDAR_HUELLAS = 2;
var iOpcion = 0;
var SUBIR_ARCHIVO = 3;
var PUBLICA_HUELLAS_IZQ = 1;
var PUBLICA_HUELLAS_DER = 2;
var PUBLICA_HUELLAS_DER_W = 3;
/*VARIABLES PARA EL NOMBRE DE LAS HUELAS*/
var curpyfechader = '';
var curpyfechaizq = '';
var curpyfechaderW = '';
/*VARIABLES PARA VALIDAR LOS CAMPOS DEL FORMULARIO*/
var bNombres = false;
var bApPaterno = false;
var bApMaterno = false;
var bFechaNacimiento = false;
var bGenero = false;
/*VARIABLES GENERALES*/
var ipoflline = '';
var iOpcionHuella = 0;
var mensajeTiempo = '';
var porcentajemin = 0;
var numerointentos = 0;
var iMilisegundos = 0;
var folioBanco = '';
var telefono = '';
var medioenvio = '';
var URL_LLAMADA_IVR = 9;
var fechaServidor = '';
var contadorConsumosWS = 0;
var ipModulo = '';
//#endregion

$(document).ready(async function () {
    
    mdlEspere("Autenticacion Trabajador");    
    urlParams = {
        iEmpleado : obtenerVarialbesURL('empleado'),
        iRenapo :   obtenerVarialbesURL('renapo'),
        cCurp :     obtenerVarialbesURL('curp')
    };

    console.log("url params:",urlParams);

    if(!isParametrosValidos(urlParams))                 // Valida que los parametros de entrada no sean vacios.
    {        
        mdlMsjFunc("Error", "Faltan parametros de entrada url", 'aceptar', '');
        // TODO: que se deberia hacer una vez se vea que faltan parametros.
        return;
    }

    ipoflline = await obtenerIpServidor();                                  // obtiene la ip del servidor offline
    var isPromotorValido = await obtenerEmpleado(urlParams.iEmpleado);    // Espera por la respuesta.
    
    if(urlParams.iEmpleado != "" && isPromotorValido)                     // Validamos que el empleado exista y que sea un promotor valido.
    {
        if(validarnumerointentos())
        {
            //obtenerIpServidor();
            await obtenerGenero();          // tienen que estar cargados los genero
            obtenerCompania();
            obtenerMedioEnvio(); 
            validaRenapo("'" + urlParams.cCurp + "'");  

            // formato de curps variables globales : Generar curp formato
            fechaServidor = consultarFechaServidor();
            curpyfechaizq = urlParams.cCurp + "_" + fechaServidor + "TEMPLATEI.txt";
            curpyfechader = urlParams.cCurp + "_" + fechaServidor + "TEMPLATED.txt";
            curpyfechaderW = urlParams.cCurp + "_" + fechaServidor + "D.txt";
            validacionHuellasTrabajador();
        }
        else{
            console.log("numero de intentos sobrepasados:",mensajeTiempo);
            detonarIne();
            /*var sBtn1       = "ACEPTAR";
            var fFunc1      = "detonarIne();";
            mdlMsjFunc(cTituloModal, mensajeTiempo, sBtn1, fFunc1);*/
        }        
    }

    iniciarEventos();      
});

//#region Funciones 

/**
 * Valida la entrada de los parametros de la url.
 * @param {*} urlParams  paramentros de entrada que contendra el iEmpleado, iRenapo, cCurp
 */
function isParametrosValidos(urlParams) 
{        
    if(
        (urlParams.iEmpleado == "" || urlParams.iEmpleado === false) ||
        (urlParams.iRenapo == "" || urlParams.iRenapo === false) ||
        (urlParams.cCurp == ""  || urlParams.cCurp === false)
    )
    {
        return false;
    }
    
    return true;
}

async function validacionHuellasTrabajador() {    
    console.log("validacion huellas trabajador");
    var opcionEjecutar = CAPTURA_INDICES_BANCOPPEL;   // OPCION 2
    iOpcion = opcionEjecutar;
    var ruta = await obtenerRutaExe(opcionEjecutar);
    var parametros = urlParams.cCurp + " " + rutas.RUTA_LOCAL_HUELLAS + " " + fechaServidor;   
    console.log("parametros:",parametros);
    ejecutaWebService(ruta, parametros);
}

async function publicahuellasder(parametros) {
    console.log("-- publicando huella derecha-- ");
	iContHuellas++;
	iOpcion = SUBIR_ARCHIVO;
    iOpcionHuella = PUBLICA_HUELLAS_DER;
    var ruta = await obtenerRutaExe(iOpcion);
    ejecutaWebService(ruta, parametros);
}

async function publicahuellasderW(parametros) {
    console.log("--- publicanco huella derecha W ---");
	iContHuellas++;
	iOpcion = SUBIR_ARCHIVO;
    iOpcionHuella = PUBLICA_HUELLAS_DER_W;
    var ruta = await obtenerRutaExe(iOpcion);
    ejecutaWebService(ruta, parametros);	
}

async function publicahuellasizq(parametros) {
    console.log("-- publicando huella izquierda ---");
	iContHuellas++;
    iOpcion = SUBIR_ARCHIVO;    
    iOpcionHuella = PUBLICA_HUELLAS_IZQ;
    var ruta = await obtenerRutaExe(iOpcion);
    ejecutaWebService(ruta, parametros);	
}

function respuestaDatos() 
{
    console.log("Cerrar BANCOPPEL");
    window.top.respuestaframe(1); //cierra el frame
}

function obtenerSexoCurp(curp)
{
    var sexCaracter = curp.charAt(10);
    if(sexCaracter == "H")          // Mujer
        return 1;
    else if(sexCaracter == "M")     // Hombre
        return 2;
}

function validarbotonesBancoppel() 
{  
        
	if (bNombres == false) {
		$("#nombre").parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
		$("#nombre").next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
	} else {
		$("#nombre").prop('disabled', true);
	}

	if (bApPaterno == false) {
		$(apPaterno).parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
		$(apPaterno).next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
	} else {
		$("#apPaterno").prop('disabled', true);
	}

	if (bApMaterno == false) {
		$(apMaterno).parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
		$(apMaterno).next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
	} else {
		$("#apMaterno").prop('disabled', true);
	}

	if (bFechaNacimiento == false) {
		$(fechaNacimiento).parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
		$(fechaNacimiento).next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
	} else {
		$("#fechaNacimiento").prop('disabled', true);
	}

	if (bGenero == false) {
		$(genero).parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
		$(genero).next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
	} else {
		$("#genero").prop('disabled', true);
	}
}

//#endregion

//#region Consultas ajax
function consultarFechaServidor()
{
    var retorno;
    $.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONSFECH" },
		success: function (data) {            
            var fecha = data.CONSFECH[0][0];
            retorno = fecha;
        }
        , error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
    });
    return retorno;
}

function validarTelefonoCorreto()
{
    // 8114863008
    var telefono = $("#numCelular").val();
    var iEmpleado = urlParams.iEmpleado;
    var cCurp = urlParams.cCurp;
    var parametros = ` '0','${telefono}' ,${iEmpleado}, '${cCurp}', 1, 0 `;
    return  enviarPost(ligaCase, { opcion: 3, idcons: "CONS0GTEL", arrdatos: parametros }).done();        
}

async function obtenerRutaExe(id) 
{
    var result = await enviarPost(ligaCase, { opcion: 3, idcons: "CONS09", arrdatos: id }).done();        
    var ruta = result.CONS09[0].liga;;      
    return ruta;
}

function validaRenapo(curp)
{
    enviarPost(ligaCase, { opcion: 3, idcons: "CONS06", arrdatos: curp }).done((result)=>{
        if(urlParams.iRenapo == 1){            
            var repo = (result.CONS06);                          
            if (urlParams.iRenapo == 1) {
                console.log(repo[0]);
                var datosNombre     = correcionCaracteres(repo[0].nombre);
                var datosPaterno    = correcionCaracteres(repo[0].paterno);
                var datosMaterno    = correcionCaracteres(repo[0].materno);
				$("#nombre").val(datosNombre);
				$("#apPaterno").val(datosPaterno);
                $("#apMaterno").val(datosMaterno);
                $("#fechaNacimiento").val(repo[0][3]);
                var sexNum = obtenerSexoCurp(urlParams.cCurp);                  // returna 1 Femenino - 2 Masculino
                $("#genero option[value="+ sexNum +"]").attr("selected",true);  // selecciona el genero
			}
        }        
    });
}

function validarnumerointentos() 
{
    console.log("validando numero intentos");
	var bResp = false;
    var arrResp = new Array();
    var curpFormateada =  "'"+urlParams.cCurp +"'";
    
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS07", arrdatos: curpFormateada },
		success: function (data) {
			arrResp = data;
            if (arrResp.CONS07[0].repuesta == 0)
                bResp = true;

            numerointentos = arrResp.CONS07[0].numerointentos;
			mensajeTiempo = arrResp.CONS07[0].mensaje;
			porcentajemin = parseFloat(arrResp.CONS07[0].porcentaje);
			iMilisegundos = arrResp.CONS07[0].milisegundos;
			
        }
        , error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});

	return bResp;
}

async function obtenerIpServidor()
{
    var result = await enviarPost(ligaCase, { opcion: 3, idcons: "CONS11" }).done();        
    var ipoflline = result.CONS11[0].ipofflinelnx;
    return ipoflline;
}

async function obtenerGenero() 
{
    var result = await enviarPost(ligaCase, { opcion: 3, idcons: "CONS0GEN" }).done();
    $("#genero").append(`<option value="" selected>SELECCIONE...</option>`);                
    var generos = result.CONS0GEN;      
    generos.forEach(genero => {
        $("#genero").append(`<option value="${genero.idgenero}">${genero.descripcion}</option>`);  
    }); 
}

function obtenerCompania() {
    $("#celCompania").append(`<option value="" selected>SELECCIONE...</option>`);   

    enviarPost(ligaCase, { opcion: 3, idcons: "CONS04" }).done((result)=>{        
        var companias = result.CONS04;      
        companias.forEach(compania => {
            $("#celCompania").append(`<option value="${compania.clavec}">${compania.nombre}</option>`);  
        });  
    });
}

function obtenerMedioEnvio() {
    $("#medioEnvio").append(`<option value="" selected>SELECCIONE...</option>`);   

    enviarPost(ligaCase, { opcion: 3, idcons: "CONS05" }).done((result)=>{        
        var medios = result.CONS05;        
        medios.forEach(medio => {
            $("#medioEnvio").append(`<option value="${medio.iestatus}">${medio.cdescripcion}</option>`);  
        });  
    });
}

async function obtenerEmpleado(empleado){
    var result = await enviarPost(ligaCase, { opcion: 3, idcons: "CONS01", arrdatos: empleado }).done();    
    var empleado = result.CONS01[0];         
    promotor = empleado;    // el promotor es un empleado

    if (result.irespuesta == 4) {
        if (empleado.icodigo == "0") {              
            $("#txtUsuario").val(empleado.cnombre);
            $("#txtNomTienda").val(empleado.ctienda);
            await obtenerDatosPaginas();
            return true;    // Si es un promotor valido
        }
    }

    return false;           // no es un promotor valido
}

async function obtenerDatosPaginas(){
    var result = await enviarPost(ligaCase, { opcion: 2 }).done();    
    datosPagina = result;   
    if (datosPagina.irespuesta == 4) {            
        $("#txtFechaDia").val(datosPagina.fechaActual);
        ipModulo = datosPagina.ipmodulo;
    }
}

function insertarDatosSms(datos) 
{
	var arrResp = new Array();
    var respuestasms = 0;
    var curp = urlParams.cCurp;

	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS03", arrdatos: datos },
		success: function (data) {            
            arrResp = (data.CONS03);
            console.log("repuesta inserta datos sms:",arrResp);            
            respuestasms = arrResp[0].respuesta;
            
            if (respuestasms == 1)      // si la respuesta es exitosa
            {                    
                console.log("valida")            ;
                if (medioenvio == 1) // llamada ivr
                {  
                    console.log("llamada ivr");
                    fninvocallamadaivr(curp);                    
				}
                else if (medioenvio == 2)   // envio de mensaje
                {
                    console.log("Envio de sms");
                    llamarPython(folioBanco);                    
				}
			}
            else 
            {
                console.log("respuesta 2");
				mdlMsj(cTituloModal, 'Promotor, se tuvo un problema al guardar los datos del teléfono, favor de contactar a mesa de ayuda!');
			}
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

function llamarPython(folioservicio) 
{
    console.log("python");
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 5, folioserv: folioservicio },
        success: function (data) 
        {
            arrResp = data;
            console.log(arrResp);

		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
    });
    mdlMsjFunc(cTituloModal, 'Autenticacion exitosa', 'aceptar', 'exito();');
}

function fninvocallamadaivr(cCurp) 
{
	var arrResp = new Array();
	var folioAutenticacion = 0;
	var arrDataEnvio = new Array();
	var Curp = "'" + cCurp.toUpperCase() + "'";
    arrDataEnvio = { opcion: 3, idcons: "CONS14", arrdatos: Curp };
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: arrDataEnvio,
		success: function (data) {
            arrResp = (data.CONS14);
            console.log("respuesta fninvocallamadaivr:", arrResp);
			folioAutenticacion = arrResp[0].respuesta;
			if (folioAutenticacion != '') {
				iOpcion = URL_LLAMADA_IVR;
				ligaCaseLlamada = obtenerligas(iOpcion);
				llamadaivr(folioAutenticacion);
			}
            else 
            {
                mdlMsj(cTituloModal, 'Colaborador, se tuvo un problema al generar el folio de autenticación INE, favor de contactar a mesa de ayuda!');
			}
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

function llamadaivr(folioAutenticacion) 
{
	var arrResp;
	$.ajax({
		async: false,
		cache: false,
		url: ligaCaseLlamada,
		type: 'GET',
		dataType: 'JSON',
		data: { telefono: telefono, codautent: folioAutenticacion, opcion: 0 },
		success: function (data) {
            arrResp = (data.estatus);
            console.log("respuesta fninvocallamadaivr:", arrResp);			
            if (data.estatus == 1) 
            {
                fnactualizarrespuestallamadaivr(folioBanco);
			}
            else 
            {
                mdlMsj(cTituloModal, 'Colaborador, se tuvo un problema al generar la llamada IVR, favor de contactar a mesa de ayuda!');
			}
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
    });
}

function fnactualizarrespuestallamadaivr(folioapi) 
{
	var arrResp = new Array();
    var actualiza = 0;
    var valores = folioapi + ",2";

	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS16", arrdatos: valores },
		success: function (data) {
            console.log(data);
			arrResp = (data.CONS16);
			actualiza = arrResp[0].respuesta;
			if (actualiza == 1) {
                mdlMsjFunc(cTituloModal, 'Autenticacion exitosa', 'aceptar', 'exito();');
			}

		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

function exito(){
    console.log("Exito de Autenticacion Bancoppel");
    var constelefono    = $("#numCelular").val();
    var conscompaniacel = $("#celCompania").val();
    console.log("constelefono->" + constelefono +  " conscompaniacel->" + conscompaniacel);
    window.top.exitoBancoppel(folioBanco,constelefono,conscompaniacel);
}

function estadoCuenta() 
{
    console.log("Detonar Estado Cuenta");
    window.top.cancelacionIne();
}

function detonarIne()
{
    console.log("Detonar autenticacion INE");
    window.top.preguntaIne();
}

function cerrarNavegadorCons() {
    console.log("Detonar cerrar navegador");
    window.top.cerrarNavegador();
}

function obtenerligas(idliga) {
	var bResp = '';
	var arrResp = new Array();
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS09", arrdatos: idliga },
		beforeSend: function () {
		}, complete: function () {
		}, success: function (data) {
			arrResp = data;
			bResp = arrResp.CONS09[0].liga;

		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
	return bResp;
}

function formatDate(date) 
{
    var newdate = date.split("-").reverse().join("-");
    return newdate;
}

function guardarDatosBancoppel() 
{ 
    
    var curp = urlParams.cCurp;
    var nombre      = $("#nombre").val().split("Ñ").join("#");
    var apPaterno   = $("#apPaterno").val().split("Ñ").join("#");
    var apMaterno   = $("#apMaterno").val().split("Ñ").join("#");
    var fechaNacimiento = $("#fechaNacimiento").val();
    var genero = $("#genero").val();    
    var iPromotor = urlParams.iEmpleado;
    var fechaNacimientoFormato = formatDate(fechaNacimiento);

    var arrDatos = {
		"cCurp": curp,
        "cNomb": nombre,
        "cPaterno": apPaterno,
        "cMaterno": apMaterno,
        "dFchNac": fechaNacimientoFormato,
        "iGenero": genero,
        "tempIzq": curpyfechaizq ,
        "tempDer": curpyfechader,
        "wsqDer": curpyfechaderW,
        "cModulo": ipModulo,
        "iPromotor": iPromotor
    };
    
    var bResp = false;
    console.log(arrDatos);

	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 4, idcons: "agregaratostbautenticacionbancoppel", arrdatos: arrDatos },
		success: function (data) {
            arrResp = (data);
            var registros = arrResp.respuesta.registros[0];
            if (arrResp.estatus == 5) 
			{
                folioBanco = registros.foliobanco;
				if (arrResp.respuesta.estatus == 1) {
                    folioBanco = registros.foliobanco;
					bResp = true;
				} 
				else {				
                    mdlMsj(cTituloModal, "Promotor, se presentó un problema al validar la ine favor de volver a intentar.");
				}
			} 
			else 
			{
				mdlMsj(cTituloModal, "consumo api fallo");
			}

		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
    });
    return bResp;
}

function validarautenticacionbancoppelws() 
{
    var iFoliosolbanco = parseInt(folioBanco); 
    var porcentaje = porcentajemin;
    var cCurp = urlParams.cCurp;
    var bResp = false;
    var arrDatos = {
        "iFolBanco": iFoliosolbanco,
        "dPorciento": porcentaje,
    };
    console.log(arrDatos);
    // alert("iFoliosolbanco->" + iFoliosolbanco);
    
    $.ajax({        
        async: false,
        cache: false,
        url: ligaCase, 
        type: 'POST',
        dataType: 'JSON', 
        data: { opcion: 4, idcons: "validarautenticacionbancoppelws", arrdatos: arrDatos },
        success: function (data) { 
            arrResp = (data);            
            var respuesta =  arrResp.respuesta;
            var registros = respuesta.registros[0][0];
            if (arrResp.estatus == 5) 
            {
                var sMensaje = "";                
                var iRepWS = parseInt(registros.respuesta);
                console.log("contador consumo-> ",contadorConsumosWS);
                console.log("intentos-> ",numerointentos);
                console.log("iRepWS-> ",iRepWS);
                console.log(registros);
                if(contadorConsumosWS <= 2){
                    switch (iRepWS) {
                        case 1:
                            console.log("Metodo de envio de codigo de autenticacion");
                            var datosinsertarsms = iFoliosolbanco + ",'" + cCurp + "'," + companiacel + "," + medioenvio + ",'" + telefono + "', 2";
                            console.log(datosinsertarsms);
                            insertarDatosSms(datosinsertarsms);
                            break;
                        case 2:
                            console.log("Metodo para recaptura de datos (validando true/false)");
                            validarDatos(1,registros);//1-recaptura de datos
                            break;                        
                        case 3:
                            console.log("Metodo para recaptura huellas (validando true/false)");
                            validarDatos(2,registros);//2-recaptura de huellas
                            break;
                        case 4:
                            //bResp = true;
                            console.log("Metodo por error del WS (mensaje de error y salir por INE)");
                            sMensaje = registros.mensaje;
                            detonarIne();
                            break;
                        case 5:
                            bResp = true;
                            console.log("Metodo por error del WS (mensaje de error y salir por INE)");
                            sMensaje = registros.mensaje;
                            break;
                        default:
                            bResp = true;
                            console.log("Error no controlado (mensaje de error y salir por INE)");
                            sMensaje = "Error no controlado.";
                            break;
                    }
                    if(bResp){
                        var sBtn1       = "ACEPTAR";
                        var fFunc1      = "cerrarNavegadorCons();";
                        mdlMsjFunc(cTituloModal, sMensaje, sBtn1, fFunc1);
                    }
                }else{
                    detonarIne();
                }
            } 
            else 
            {
				mdlMsj(cTituloModal, "consumo api fallo auntenticacionIne 2");
			}
        },
        error: function (a, b, c) {
            alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
        }   
    });
}

function validarDatos(opcion,registros) 
{
    // cambiamos los estilos de los campos no validos.
    bNombres    = registros.valnombre == "t" ? true : false;
    bApPaterno  = registros.valnombre == "t" ? true : false;
    bApMaterno  = registros.valnombre == "t" ? true : false;
    bFechaNacimiento    = registros.valnombre == "t" ? true : false;
    bGenero     = registros.valnombre == "t" ? true : false;
    validarbotonesBancoppel();
    if(opcion == 1){
        recapturadatosine();
    }
    else if(opcion == 2)
    {
        recapturahuellas();
    }
}

function mensajeError(error)
{
    console.log(error);
    mdlMsjCerrar("Error", error);
}


function recapturadatosine() 
{
	mdlEspere(cTituloModal);
	iContHuellas = 0;
	parametrosHuellas = curpyfechaizq + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_INE + " " + ipoflline + " 0";	
	publicahuellasizq(parametrosHuellas);	
}

function recapturahuellas() 
{
	mdlEspere(cTituloModal);
	validacionHuellasTrabajador();	
}

/**
 * Retorna Una promesa de ajax. de forma asyncrona nos retorna una valor.
 * @param {*} url donde se enviará la información
 * @param {*} data que será enviada. usualmente se envia un objeto con la opcion del case.
 */
function enviarPost(url,data) {
	return $.ajax({
		url: url,
		cache:false,
		type: "POST",
        data: data,
        dataType: 'JSON',
	})
	.done((result)=>{ return result; })
	.fail(()=> { console.log("fallo el envio") });
}
//#endregion

//#region Eventos
/**
 * punto de partida de los eventos.
 */
function iniciarEventos()
{
    clickFecha("#fechaNacimiento");         
    crearValidacionesForm("#formVal", 
        {                               // reglas
            nombre: { required: true, minlength: 1, maxlength: 40 },
            apPaterno: { required: true, minlength: 1, maxlength: 40 },
            apMaterno: {minlength: 1, maxlength: 40},
            fechaNacimiento: { required: true, minlength: 1 },
            genero: { required: true, minlength: 1 },
            numCelular: { required: true, minlength: 10, maxlength: 10 },
            numCelularConfirmacion: { required: true, minlength: 10, maxlength: 10 },
            celCompania: { required: true, minlength: 1 },
            medioEnvio: { required: true, minlength: 1 }
        },
        // TODO: Implementar funcion de callback
        function (form) {// function callback
            
            mdlEspere("Autenticacion Trabajador");
            validarTelefonoCorreto().done((result)=>{                       
                var respuesta = result.CONS0GTEL[0];
                telefono = $("#numCelular").val();
                medioenvio = $("#medioEnvio").val();
                companiacel = $("#celCompania").val();
                if(respuesta.identificador == "0")      // Si es un numero valido
                {
                    if(contadorConsumosWS < numerointentos ){
                        contadorConsumosWS++;
                        console.log("incrementa contador:", contadorConsumosWS);
                        if(guardarDatosBancoppel())
                        {
                            setTimeout(()=> {
                                validarautenticacionbancoppelws();
                            }, iMilisegundos);
                        }
                    }else{
                        console.log("limite de intentos excedido");
                        detonarIne();
                    }
                }
                else    // si no es un numero valido mostrar mensaje de recaptura.
                {                    
                    mdlDesbloquearMsj();
                    $('#alertcorreo').remove('');
                    $("#insert-message").append("<div id = 'alertcorreo' class='form-group col-md-4 formulario alert alert-danger'>El Teléfono celular capturado no es correcto o fue utilizado anteriormente para otra afiliación, favor de verificarlo e intentar de nuevo</div>  ");            
                    resetearCelulares();

                   /* setTimeout(()=>{
                        $('#alertcorreo').remove('');
                    }, 8000); */                   
                }

            }); 

        }
    );
    
    $("#numCelular").focus((e)=>{
        $('#alertcorreo').remove('');
    });

    $("#numCelularConfirmacion").focus((e)=>{
        $('#alertcorreo').remove('');
    });

    $("#numCelular").keydown((e)=>{
        evaluarSoloNumeros(e);
    });

    $("#numCelularConfirmacion").keydown((e)=>{
        evaluarSoloNumeros(e);                        
    }); 

    $("#numCelularConfirmacion").keyup((e)=>{ 
        validarConfirmacionCelular();        
    }); 

    $("#fechaNacimiento").keydown((e) =>{
        var keyCode = e.keyCode;
        if (keyCode == 8)                           // tecla retroceso     
        {            
            $("#fechaNacimiento").val("");          // Limpiamos la fecha.    
        }
        e.preventDefault();
    });

    $('#btncancelar').click(function (){
        var sMensaje  = "¿Deseas cancelar la Autenticación Bancoppel?";
        var sBtn1     = "NO";
        var fFunc1    = "mdlDesbloquearMsj();";
        var sBtn2     = "SI";
        var fFunc2     = "detonarIne();";
        mdlMsjFuncPreg(cTituloModal, sMensaje, sBtn1, fFunc1, sBtn2, fFunc2);
    });
}

/**
 * Da el formato a mostrar en los datepicker, ademas los prepara para mostrarse.
 * @param {inputId} valor a afectar por id de jquery-> "#ejemplo"
 */
function clickFecha(inputId)
{
    var fecha = new Date();
    var fechaMin = fecha.getFullYear() - 100;
    var fechaMax = fecha.getFullYear() -18;
    $(inputId).datepicker({
        //options
        changeMonth: true,
        //Para poder cambiar el año
        changeYear: true,
        //Asigna el rango de años que apareceran
        yearRange: fechaMin+":"+fechaMax,
        //Asigna los nombres del mes
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul','Ago', 'Sep', 'Oct','Nov', 'Dic'],
        //Formato de la fecha.
        dateFormat: 'dd-mm-yy',        
        //Nombre corto de los dias.
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        defaultDate: "01-01-"+fechaMax
    });
	$(inputId).click(() =>{
		$(inputId).datepicker('show');
    });

    inHabilitarCampos();

}

function resetearCelulares() {
    $('#numCelular').val('');
    $('#numCelularConfirmacion').val('');  

    $('#numCelular').parent().parent().html(`
        <label class="control-label col-md-12 padding-top "><i class="fa fa-calculator padding-right"></i>
            *Teléfono celular :</label>
        <div class="col-md-12 input-val">
            <input type="password" maxlength="10" class="form-control form-control-sm" name="numCelular" id ="numCelular"/>
        </div>
        `);

    $('#numCelularConfirmacion').parent().parent().html(`
        <label class="control-label col-md-12 padding-top"><i class="fa fa-calculator padding-right"></i>
            *Confirmación teléfono celular :</label>
        <div class="col-md-12 input-val ">
            <input type="text" maxlength="10" class="form-control form-control-sm" name="numCelularConfirmacion" id="numCelularConfirmacion" />
        </div>
    `);

    $("#numCelular").keydown((e)=>{
        evaluarSoloNumeros(e);
    });

    $("#numCelularConfirmacion").keydown((e)=>{
        evaluarSoloNumeros(e);                        
    }); 

    $("#numCelularConfirmacion").keyup((e)=>{ 
        validarConfirmacionCelular();        
    });     


    $("#numCelular").focus((e)=>{
        $('#alertcorreo').remove('');
    });

    $("#numCelularConfirmacion").focus((e)=>{
        $('#alertcorreo').remove('');
    });

}

function habilitarCampos()  // "telefono","confirmacion","compañia","medio envio"
{
    $("#numCelular").attr("disabled",false);
    $("#numCelularConfirmacion").attr("disabled",false);    
    $("#celCompania").attr("disabled",false);        
    $("#medioEnvio").attr("disabled",false); 
}

function inHabilitarCampos()  // "telefono","confirmacion","compañia","medio envio"
{
    $("#numCelular").attr("disabled",true);
    $("#numCelularConfirmacion").attr("disabled",true);    
    $("#celCompania").attr("disabled",true);        
    $("#medioEnvio").attr("disabled",true);            
}

//#endregion

//#region Validaciones
/**
 * 
 * @param {*} formId id de formulario para jquery -> "#ejemplo"
 * @param {*} rules objetos de reglas a validar.
 * @param {*} callback funcion callback para trabajar con la funcion en cada implementacion.
 */
function crearValidacionesForm(formId, rules, callback)
{
    $.validator.setDefaults({
		submitHandler:callback
    });  

    $(formId).validate({
        rules: rules,        					
		errorElement: "small",
		errorPlacement: function (error, element) {
            error.addClass("help-block-jqval col-md-12");
			element.parents(".input-val").addClass("has-feedback-jqval");
			if (element.prop("type") === "checkbox" || element.prop("type") === "radio") { error.insertAfter(element.parent("label")); }
			else { error.insertAfter(element); }			
            if (!element.next("span")[0]) { $("<span class='fa fa-times feedback-jqval'></span>").insertAfter(element); } 
		},
		success: function (element) {            
            if (!element.next("span")) { $("<span class='fa fa-check feedback-jqval'></span>").insertAfter(element); }
		},
		highlight: function (element) {
			$(element).parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
			$(element).next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
		},
		unhighlight: function (element) {
			$(element).parents(".input-val").removeClass("has-error-jqval").addClass("has-success-jqval");
			$(element).next("span").removeClass("fa-times").addClass("fa-check");
		}
    }); 
}

/**
 * Se encarga de verificar que lo que se escribe en el input sea solo numerico.
 * @param {event} event evento de los input de tipo text.
 */
function evaluarSoloNumeros(event) {
	var keyCode = event.keyCode; //

	if (
		(keyCode >= 48 && keyCode <= 57) 					// numeros de teclado
		|| (keyCode >= 96 && keyCode <= 105) 				// numeros de teclado	
		|| keyCode == 8										// retroceso	
		|| keyCode == 9										// tab	
	) 
	{
		return;
	}
	event.preventDefault();									// previene que se escriba cualquier otra tecla fuera de las condicionadas
}

function validarConfirmacionCelular()               
{
    var confirmacion = $("#numCelularConfirmacion").val();
    $('#alertcorreo').remove('');
    if( confirmacion.length == 10){
        var celCapt = $('#numCelular').val();
        if (celCapt == confirmacion) {
            $('#numCelularConfirmacion').focusout();
        }
        else {
            $('#numCelularConfirmacion').attr('type', 'text');
            $('#numCelularConfirmacion').val('');                
            $("#insert-message").append("<div id = 'alertcorreo' class='form-group col-md-4 formulario alert alert-danger'>El teléfono celular capturado no coincide</div>  ");            
            resetearCelulares();
           /* setTimeout(()=>{
                $('#alertcorreo').remove('');
            }, 8000);*/
        }
    }
}
//#endregion

//#region Funciones de webservice

function ejecutaWebService(sRuta, sParametros)
{
    console.log("sRuta-> " + sRuta + " sParametros-> " + sParametros);
    soapData = "",
        httpObject = null,
        docXml = null,
        iEstado = 0,
        sMensaje = "";
    sUrlSoap = "http://127.0.0.1:20044/";
    soapData =
        '<?xml version=\"1.0\" encoding=\"UTF-8\"?>' +
        '<SOAP-ENV:Envelope' +
        ' xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"' +
        ' xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"' +
        ' xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"' +
        ' xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"' +
        ' xmlns:ns2=\"urn:ServiciosWebx\">' +
        '<SOAP-ENV:Body  SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">' +
        '<ns2:ejecutarAplicacion>' +
        '<inParam>' +
        '<Esperar>1</Esperar>' +
        '<RutaAplicacion>' + sRuta + '</RutaAplicacion>' +
        '<parametros><![CDATA[' + sParametros + ']]></parametros>' +
        '</inParam>' +
        '</ns2:ejecutarAplicacion>' +
        '</SOAP-ENV:Body>' +
        '</SOAP-ENV:Envelope>';
    httpObject = getHTTPObject();

    if (httpObject) {
        if (httpObject.overrideMimeType) {
            httpObject.overrideMimeType("false");
        }        
        httpObject.open('POST', sUrlSoap, false); //-- no asincrono
        httpObject.setRequestHeader("Accept-Language", null);
        httpObject.onreadystatechange = function () {
            if (httpObject.readyState == 4 && httpObject.status == 200) {
                parser = new DOMParser();
                docXml = parser.parseFromString(httpObject.responseText, "text/xml");
                iEstado = docXml.getElementsByTagName('Estado')[0].childNodes[0].nodeValue;                                                            
                $("#divContIne").show();
                // llamada a la función que recibe la respuesta del WebService                                        
                respuestaWebService(iEstado);
            }
            else {
                console.log(httpObject.status);
            }
        };            
        httpObject.send(soapData);
    }
}

function getHTTPObject() {
	var xhr = false;
	if (window.ActiveXObject) {
		try { xhr = new ActiveXObject("Msxml2.XMLHTTP"); }
		catch (e) {
			try { xhr = new ActiveXObject("Microsoft.XMLHTTP"); }
			catch (e) { xhr = false; }
		}
	} else if (window.XMLHttpRequest) {
		try { xhr = new XMLHttpRequest(); }
		catch (e) { xhr = false; }
	}
	return xhr;
}

function respuestaWebService(iRespuesta) 
{
    console.log('iopcion ->' + iOpcion + ' irespuesta ->' + iRespuesta);
	switch (iOpcion) {
		case VALIDAR_HUELLAS:
			if (iRespuesta == 1) {
				iContHuellas = 0;
                parametrosHuellas = curpyfechaizq + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_INE + " " + ipoflline + " 0";
                console.log(parametrosHuellas);
				publicahuellasizq(parametrosHuellas);
			} else if (iRespuesta == 99) {
                estadoCuenta();
			} else {
                var mensajefnc = 'Promotor, se presentó un problema al capturar las huellas de los dedos índices del Trabajador, favor de capturar nuevamente.';
                mdlMsjFunc(cTituloModal, mensajefnc, 'Aceptar', 'recapturahuellas();');
			}
			break;

		case SUBIR_ARCHIVO:
			switch (iOpcionHuella) {
				case PUBLICA_HUELLAS_IZQ:
					if (iRespuesta == 1) {
						iContHuellas = 0;
                        parametrosHuellas = curpyfechader + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_INE + " " + ipoflline + " 0";                        
						publicahuellasder(parametrosHuellas);
					}
					else {
						if (iContHuellas < 3) {
							parametrosHuellas = curpyfechaizq + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_INE + " " + ipoflline + " 0";
							publicahuellasizq(parametrosHuellas);
						}
						else {
							mdlMsjFunc(cTituloModal, 'Promotor, se presentó un problema al capturar las huellas de los dedos índices del Trabajador, favor de capturar nuevamente.', 'aceptar', 'recapturahuellas();');
						}
					}
					break;
				case PUBLICA_HUELLAS_DER:   
					if (iRespuesta == 1) {
                        iContHuellas = 0;
                        // TODO: validar huella derecha W
                        parametrosHuellas = curpyfechaderW + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_INE + " " + ipoflline + " 0";                        
                        publicahuellasderW(parametrosHuellas);
					}
					else {
						if (iContHuellas < 3) {
							parametrosHuellas = curpyfechader + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_INE + " " + ipoflline + " 0";
							publicahuellasder(parametrosHuellas);
						}
						else {
							mdlMsjFunc(cTituloModal, 'Promotor, se presentó un problema al capturar las huellas de los dedos índices del Trabajador, favor de capturar nuevamente.', 'aceptar', 'recapturahuellas();');
						}
                    }
                    break;
                    
                case PUBLICA_HUELLAS_DER_W:   
                    if (iRespuesta == 1) {
                        iContHuellas = 0;
                        habilitarCampos();      // habilita los campos "telefono","confirmacion","compañia","medio envio
                        mdlDesbloquearMsj();    // quita los modales          
                    }
                    else {
                        if (iContHuellas < 3) {
                            parametrosHuellas = curpyfechader + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_INE + " " + ipoflline + " 0";
                            publicahuellasder(parametrosHuellas);
                        }
                        else {
                            mdlMsjFunc(cTituloModal, 'Promotor, se presentó un problema al capturar las huellas de los dedos índices del Trabajador, favor de capturar nuevamente.', 'aceptar', 'recapturahuellas();');
                        }
                    }
                break;

				default:
					break;
			}
			break;

		default:
			break;
    }
}
//#endregion